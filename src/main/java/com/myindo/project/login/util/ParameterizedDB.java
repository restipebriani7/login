package com.myindo.project.login.util;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import com.myindo.project.login.model.ParameterDB;


public class ParameterizedDB {
	public static ParameterDB getPropeties() {
		ParameterDB parameterDB = new ParameterDB();
		Properties prop = new Properties();
		InputStream input = null;

		try {
			input = new FileInputStream("/etc/config-server.properties");
			prop.load(input);
			
			parameterDB.setEndPoint(prop.getProperty("postgre.endpoint"));
			parameterDB.setDb(prop.getProperty("postgre.db"));
			parameterDB.setUsername(prop.getProperty("postgre.username"));
			parameterDB.setPassword(prop.getProperty("postgre.password"));
			parameterDB.setPort(prop.getProperty("postgre.port"));
		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return parameterDB;
	}
}
