/**
 * 
 */
package com.myindo.project.login.model;

/**
 * @author Resti Pebriani
 *
 */
public class ResponseContentModel {
	private String npk;
	private String nama;
	public String getNpk() {
		return npk;
	}
	public void setNpk(String npk) {
		this.npk = npk;
	}
	public String getNama() {
		return nama;
	}
	public void setNama(String nama) {
		this.nama = nama;
	}
}
